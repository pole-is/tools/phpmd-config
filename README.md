# irstea/phpmd-config

Jeux de règles pour [PHP Mess Detector](https://phpmd.org/).

### Installation

```shell
composer require --dev irstea/phpmd-config
```

### Configuration

Le package fournit plusieurs jeux de règles :

- `loose` : pour des projets existants.
- `strict` : recommandé pour les nouveaux projets.

Le jeu sélectionné doit être référencé dans la configuration de PHPMD :

```xml
<ruleset>

    <!-- ... -->

    <rule ref="vendor/irstea/phpmd-config/strict.xml" />

    <!-- ... -->

</ruleset>
```
